const User = require('../models/User');
const Product = require('../models/Product');
const auth = require('../auth');
const bcrypt = require('bcrypt');


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		address: reqBody.address,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => {
		if (error) {
			console.log(error);	// to be removed
			return false;
		}
		else {
			
			return "User has been successfully created!";
		}
	})
};

module.exports.loginUser = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result == null) {
			
			return "Error! User does not exist!";
		}
		else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if (isPasswordCorrect) {
				console.log("User has successfully logged in.");	// to be removed
				return { accessToken: auth.createAccessToken(result.toObject()) };
			}
			else {
				
				return "Error! Password does not match!";
			}
		}
	})
};

module.exports.checkUserDuplicate = (reqBody) => {
	return User.find( { email: reqBody.email } ).then(result => {
		if (result.length > 0) {
			
			return "User already exists!";
		}
		else {
			
			return "User is not registered.";
		}
	})
};

// module.exports.getAllUser = () => {
// 	return User.find({}).then(result => {
// 		return result;
// 	}) 
// };
module.exports.getAllUsers = async (adminData) => {
	if(adminData.isAdmin){
		return User.find({}, {password: 0}).then(result => {
			result.password = " ";
			console.log(result);
			return result;
		})
	}
	else{
		return (`You have no Admin access`);
	}
};


module.exports.userOrders = (reqParams) => {
	return User.findById(reqParams.userId).then((result, error) => {
		if (error) {
			
			return "Error! Cannot get user's orders!";
		}
		else {
			console.log("Retrieved user's orders successfully.");	// to be removed
			return result.orderList;
		}
	})
};

module.exports.userCheckout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orderList.push( { productId: data.productId } )

		return user.save().then((user, error) => {
			if (error) {
				
				return "Error! Product was not ordered successfully by the user!";
			}
			else {
				
				return "User has successfully ordered a product.";
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.orderedBy.push( { userId: data.userId } )

		return product.save().then((product, error) => {
			if (error) {
				
				return "Error! User was not able to order the product!";
			}
			else {
				
				return "Product was ordered by the user.";
			}
		})
	})

	if (isUserUpdated && isProductUpdated) {
		
		return "User's check out products have been successfully updated!";
	}
	else {
		
		return "User's checkout update failed!";
	}
};

module.exports.setAsAdmin = async (reqParams, adminData) => {
	//find out if requester is admin
	if(adminData.isAdmin){
		return User.findByIdAndUpdate(reqParams.userId, {isAdmin: true}).then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true, "User is set to admin!";
			}
		})
	}
	else{
		return(`You have no admin access`);
	}
}