const Product = require('../models/Product');


module.exports.addProduct = (reqBody) => {
	if (reqBody.isAdmin) {
		let newProduct = new Product({
			name: reqBody.product.name,
			description: reqBody.product.description,
			price: reqBody.product.price
		})

		return newProduct.save().then((result, error) => {
			if (error) {
				
				return "Error! Product not added successfully!";
			}
			else {
				
				return "Product is successfully added.";
			}
		})
	}
	else {
		
		return "Error! User is not and admin!";
	}
};

module.exports.getAllProducts = () => {
	return Product.find( {} ).then((result, error) => {
		if (error) {
			
			return "Error! Cannot get all products!";
		}
		else {
			console.log("All products have been successfully retrieved.");
			return result;
		}
	})
};

module.exports.getAllActiveProducts = () => {
	return Product.find( { isActive: true } ).then((result, error) => {
		if (error) {
			
			return "Error! Cannot get all active products!";
		}
		else {
			console.log("Active products have been successfully retrieved.");	// to be removed
			return result;
		}
	})
};

module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then((result, error) => {
		if (error) {
			
			return "Error! Cannot get specific product!";
		}
		else {
			console.log("Product has been successfully retrieved.");
			return result;
		}
	})
};

module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		if (error) {
			
			return "Error! Cannot update product information!";
		}
		else {
			
			return "Product information has been successfully updated.";
		}
	})
};

module.exports.archiveProduct = (reqParams) => {
	let updatedProduct = {
		isActive: false
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		if (error) {
			
			return "Error! Cannot archive product !";
		}
		else {
			
			return "Product was successfully archived.";
		}
	})
};

module.exports.unarchiveProduct = (reqParams) => {
	let updatedProduct = {
		isActive: true
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, error) => {
		if (error) {
			
			return "Error! Cannot update product availability!";
		}
		else {
			
			return "Product availability was successfully updated.";
		}
	})
};