const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');


router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));
});

router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

router.post('/checkEmail', (req, res) => {
	userController.checkUserDuplicate(req.body).then(result => res.send(result));
});



// router.get('/getAllUser',auth.verify,(req, res) => {
//    userController.getAllUser().then((resultFromController) => res.send(resultFromController));
router.get("/all", auth.verify, (req, res) => {

	const adminData = auth.decode(req.headers.authorization);

	userController.getAllUsers(adminData).then(resultFromController => res.send(resultFromController));
});



router.get('/:userId/orders', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	if (data) {
		userController.userOrders(data).then(result => res.send(result));
	}
	else {
		
		res.send("Error! User not authenticated!");
	}
});

router.post('/checkout', auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	if (data) {
		userController.userCheckout(data).then(result => res.send(result));
	}
	else {
		
		res.send("Error! User not authenticated!");
	}
});

router.put("/:userId/admin", auth.verify, (req,res) => {

	const adminData = auth.decode(req.headers.authorization);

	userController.setAsAdmin(req.params, adminData).then(resultFromController => res.send(resultFromController));
})



module.exports = router;